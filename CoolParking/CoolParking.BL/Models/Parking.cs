﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.


using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    class Parking
    {
        private static Parking _instance;
        public decimal Balance { get; set; }
        public List<Vehicle> ParkedVehicles { get; set; }
        private Parking() 
        {
            Balance = Settings.InitialParkingBalance;
            ParkedVehicles = new List<Vehicle>(0);
        }
        public static Parking GetInstance()
        {
            if (_instance == null)
            {
                _instance = new Parking();
            }
            return _instance;
        }  
       
    }
}