﻿// TODO: implement class Vehicle.+
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).+
//       The format of the identifier is explained in the description of the home task.+
//       Id and VehicleType should not be able for changing.+
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.+
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.+
using System;
using System.Text.RegularExpressions;
using System.Threading;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        private static readonly string _takeLetter = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        private static readonly string _takeDigit = "0123456789";
        private static long _lastId = DateTime.UtcNow.Ticks;


        public string Id { get; }
        public VehicleType VehicleType { get; }
        public decimal Balance { get; private set; }


        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            if(id==null)
            {
                throw new ArgumentException("Wrong format of id for new Vehicle. Your input: null");
            }    
            if (!Regex.IsMatch(id, "^[A-Z]{2}-[0-9]{4}-[A-Z]{2}"))
            {
                throw new ArgumentException("Wrong format of id for new Vehicle. Your input: {id}");
            }
            else if (balance < 0)
            {
                throw new ArgumentException("Vehicle can't have negative balance on creation");
            }
            else if (!Enum.IsDefined(typeof(VehicleType), vehicleType))
            {
                throw new ArgumentException("Wrong Vehicle type.");
            }
                
            this.Id = id;
            this.VehicleType = vehicleType;
            this.Balance = balance;

        }
        public void AddToBalance(decimal amount)
        {
            if (amount <= 0) 
            {
                throw new ArgumentException("You can't top up on amount <= 0");
            }
            this.Balance += amount;
        }

        public decimal SendMoneyForParking()
        {
            decimal price = Settings.VehicleTariff(this.VehicleType);
            if (price < Balance)
            {
                Balance -= price;
                return price;
            }
            else if(Balance<0)
            {
                decimal diff = price* Settings.FineRatio;
                Balance -= diff;
                return diff;
            }
            else
            {
                decimal diff = price - Balance;
                decimal extraPay = diff * Settings.FineRatio;
                Balance = -extraPay;
                return (price-diff) + extraPay;
            }
        }

        private static string GenerateId(long id)
        {
            string number = "";

            number += _takeLetter[(int)(id >> 5) & (_takeLetter.Length - 1)];
            number += _takeLetter[(int)(id >> 10) & (_takeLetter.Length - 1)];
            number += "-";
            number += _takeDigit[(int)(id >> 15) & (_takeDigit.Length - 1)];
            number += _takeDigit[(int)(id >> 20) & (_takeDigit.Length - 1)];
            number += _takeDigit[(int)(id >> 25) & (_takeDigit.Length - 1)];
            number += _takeDigit[(int)(id >> 30) & (_takeDigit.Length - 1)];
            number += "-";
            number += _takeLetter[(int)(id >> 35) & (_takeLetter.Length - 1)];
            number += _takeLetter[(int)(id >> 40) & (_takeLetter.Length - 1)];

            return number;
        }
        public static string GenerateRandomRegistrationPlateNumber()
        {
            _lastId = (_lastId + ((_lastId / 100) * 5));
            return GenerateId(_lastId);
        }

        public override string ToString()
        {
            return $"Vehicle Id :{Id}. Vehicle type {VehicleType}. Current balance = {Balance}.";
        }
    }
}