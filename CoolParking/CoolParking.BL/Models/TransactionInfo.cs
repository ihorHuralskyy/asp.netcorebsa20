﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.
using System;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        
        public string VehicleId { get; set; }
       
        public decimal Sum { get; set; }

        public DateTime TransactionDate { get; set; }
        public TransactionInfo(string id,decimal am, DateTime traTime)
        {
            TransactionDate = traTime;
            VehicleId = id;
            Sum = am;
        }
        public string GetTransactionInfo()
        {
            return $"{TransactionDate.Date} {TransactionDate.TimeOfDay}: {Sum} money withdrawn from vehicle with Id='{VehicleId}'.";
        }
    }
}