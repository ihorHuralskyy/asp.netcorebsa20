﻿using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.BL.Requests
{
    public class CreateNewVehicle
    {
        public string Id { get; set; }
        public VehicleType VehicleType { get; set; }
        public decimal Balance { get; set; }
    }
}
