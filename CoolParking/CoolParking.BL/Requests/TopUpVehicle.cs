﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.BL.Requests
{
    public class TopUpVehicle
    {
        public string Id { get; set; }
        public decimal Sum { get; set; }
    }
}
