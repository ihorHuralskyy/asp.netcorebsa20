﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Text.RegularExpressions;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private Parking _parking;
        private ITimerService _withdrawTimer;
        private ITimerService _logTimer;
        private ILogService _logService;
        private List<TransactionInfo> _currentTransactions;
        public ParkingService(ITimerService withdrawTimer,ITimerService logTimer,ILogService logService)
        {
            _parking = Parking.GetInstance();
            _withdrawTimer = withdrawTimer;
            _logTimer = logTimer;
            _logService = logService;
            _currentTransactions = new List<TransactionInfo>(0);
            _withdrawTimer.Elapsed += WithdrawPayment;
            _logTimer.Elapsed += WriteToLog;
            _withdrawTimer.Start();
            _logTimer.Start();
        }
        
        
        public decimal GetBalance()
        {
            return _parking.Balance;
        }

        public int GetCapacity()
        {
            return Settings.ParkingCapacity;
        }
        public int GetFreePlaces()
        {
            return Settings.ParkingCapacity - _parking.ParkedVehicles.Count;
        }
        public ReadOnlyCollection<Vehicle> GetVehicles() => _parking.ParkedVehicles.AsReadOnly();

        public Vehicle GetVehicle(string vehicleId)
        {
            if (!Regex.IsMatch(vehicleId, "^[A-Z]{2}-[0-9]{4}-[A-Z]{2}"))
            {
                throw new ArgumentException($"Wrong format of id. Your input: {vehicleId}");
            }
            foreach (var vehicle in _parking.ParkedVehicles)
            {
                if (vehicle.Id == vehicleId)
                {
                    return vehicle;
                }
            }
            throw new KeyNotFoundException($"Parking doesn't have vehicle with id {vehicleId}");
        }
        public void AddVehicle(Vehicle newVehicle)
        {
            if (_parking.ParkedVehicles.Count >= Settings.ParkingCapacity)
            {
                throw new InvalidOperationException($"Parking is full");
            }
            foreach (var vehicle in _parking.ParkedVehicles)
            {
                if (vehicle.Id == newVehicle.Id)
                {
                    throw new ArgumentException($"Parking already has vehicle with id {vehicle.Id}");
                }
            }
            _parking.ParkedVehicles.Add(newVehicle);
        }
        public void RemoveVehicle(string vehicleId)
        {
            if (!Regex.IsMatch(vehicleId, "^[A-Z]{2}-[0-9]{4}-[A-Z]{2}"))
            {
                throw new ArgumentException($"Wrong format of id. Your input: {vehicleId}");
            }
            foreach (var vehicle in _parking.ParkedVehicles)
            {
                if (vehicle.Id == vehicleId)
                {
                    if (vehicle.Balance < 0)
                    {
                        throw new InvalidOperationException($"You have to pay your debt");
                    }
                    _parking.ParkedVehicles.Remove(vehicle);
                    return;
                }
            }
            throw new KeyNotFoundException($"Parking doesn't have vehicle with id {vehicleId}");
        }
        
        public void TopUpVehicle(string id, decimal amount)
        {
            if (Regex.IsMatch(id, "^[A-Z]{2}-[0-9]{4}-[A-Z]{2}"))
            {
                foreach (var vehicle in _parking.ParkedVehicles)
                {
                    if (vehicle.Id == id)
                    {
                        vehicle.AddToBalance(amount);
                        return;
                    }
                }
                throw new ArgumentException($"Parking doesn't have vehicle with id {id}");
            }
            else
            {
                throw new ArgumentException($"Wrong format of id. Your input: {id}");
            }
        }

        public Vehicle TopUpVehicleForAPI(string id, decimal amount)
        {
            if (!Regex.IsMatch(id, "^[A-Z]{2}-[0-9]{4}-[A-Z]{2}"))
            {
                  throw new ArgumentException($"Wrong format of id. Your input: {id}");
            }
            foreach (var vehicle in _parking.ParkedVehicles)
            {
                if (vehicle.Id == id)
                {
                    vehicle.AddToBalance(amount);
                    return vehicle;
                }
            }
            throw new KeyNotFoundException($"Parking doesn't have vehicle with id {id}");
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return _currentTransactions.ToArray();
        }
        public string ReadFromLog()
        {
            return _logService.Read();
        }

        private void WithdrawPayment(object sender, ElapsedEventArgs e)
        {
            foreach(var vehicle in _parking.ParkedVehicles)
            {
                decimal payment = vehicle.SendMoneyForParking();
                _parking.Balance += payment;
                _currentTransactions.Add(new TransactionInfo( vehicle.Id, payment, DateTime.Now));
            }
        }

        private void WriteToLog(object sender, ElapsedEventArgs e)
        {
            string log = "";
            foreach (var transaction in _currentTransactions)
            {
                log += transaction.GetTransactionInfo()+"\n";
            }
            _logService.Write(log);
            _currentTransactions = new List<TransactionInfo>(0);
        }

        public void Dispose()
        {
            _parking.Balance = Settings.InitialParkingBalance;
            _parking.ParkedVehicles = new List<Vehicle>(0);
        }
    }
}