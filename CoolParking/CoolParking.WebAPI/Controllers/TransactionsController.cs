﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Requests;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class TransactionsController : Controller
    {
        private readonly IParkingService _parkingService;
        public TransactionsController(IParkingService service)
        {
            _parkingService = service;
        }

        [HttpGet]
        public IEnumerable<TransactionInfo> Last()
        {

            return _parkingService.GetLastParkingTransactions();
        }

        [HttpGet]
        public ActionResult<string> All()
        {
            try
            {
                var res = _parkingService.ReadFromLog();
                return Ok(res);
            }
            catch (InvalidOperationException e)
            {
                return NotFound(e.Message);
            }
            
        }


        [HttpPut]
        public ActionResult<Vehicle> TopUpVehicle(TopUpVehicle top)
        {
            try
            {
                var veh = _parkingService.TopUpVehicleForAPI(top.Id,top.Sum);
                return Ok(veh);
            }
            catch(ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            catch(KeyNotFoundException e)
            {
                return NotFound(e.Message);
            }
        }
    }
}