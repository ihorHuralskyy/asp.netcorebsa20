﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using Microsoft.AspNetCore.Mvc;


namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ParkingController : Controller
    {
        private readonly IParkingService _parkingService;
        public ParkingController(IParkingService service)
        {
            _parkingService = service;
        }

        
        [HttpGet]
        public decimal Balance()
        {
            return _parkingService.GetBalance();
        }

        [HttpGet]
        public int Capacity()
        {
            return _parkingService.GetCapacity();
        }

        [HttpGet]
        public int FreePlaces()
        {
            return _parkingService.GetFreePlaces();
        }
    }
}