﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Requests;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : Controller
    {

        private readonly IParkingService _parkingService;
        public VehiclesController(IParkingService service)
        {
            _parkingService = service;
        }

        //GET api/vehicles
        [HttpGet]
        public IEnumerable<Vehicle> GetVehicles()
        {
            
            return _parkingService.GetVehicles();
        }

        //GET api/vehicles
        [HttpGet("{id}")]
        public ActionResult<Vehicle> GetVehicle(string id)
        {
            try
            {
                var veh = _parkingService.GetVehicle(id);
                return Ok(veh);
            }
            catch(ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            catch(KeyNotFoundException e)
            {
                return NotFound(e.Message);
            }
        }


        [HttpPost]
        public ActionResult<Vehicle> AddVehicle(CreateNewVehicle newVehicle)
        {
            try
            {
                _parkingService.AddVehicle(new Vehicle(newVehicle.Id, newVehicle.VehicleType, newVehicle.Balance));
                return Created("",newVehicle);
            }
            catch(Exception e)
            {
                return BadRequest(e.Message);
            }
            
        }

        [HttpDelete("{id}")]
        public ActionResult<Vehicle> RemoveVehicle(string id)
        {
            try
            {
                _parkingService.RemoveVehicle(id);
                return NoContent();
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            catch (KeyNotFoundException e)
            {
                return NotFound(e.Message);
            }
            catch(InvalidOperationException e)
            {
                return Forbid(e.Message);
            }
        }
    }
}