﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using System;
using System.IO;
using System.Reflection;
using System.Linq;
using System.Net.Http;
using CoolParking.BL.Requests;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace ParkingInterface
{
    class Program
    {
        private static HttpClient _client;
        static async Task Main(string[] args)
        {
            _client = new HttpClient();
            bool endApp = false;
            Console.WriteLine("Cool parking !!!!!\n");
            
            var withdrawTimer = new TimeService(Settings.PaymentPeriod);
            var logTimer = new TimeService(Settings.LogWritingPeriod); 
            var logService = new LogService($@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log");
            ParkingService parkingService = new ParkingService(withdrawTimer,logTimer,logService);
            while (!endApp)
            {
                Console.WriteLine("\n\n\tMenu:\n");
                Console.WriteLine("1.Press 1 to display current balance of the parking.");
                Console.WriteLine("2.Press 2 to get capacity.");
                Console.WriteLine("3.Press 3 to display number of free parking places.");
                Console.WriteLine("4.Press 4 to display all Vehicle that are on parking.");
                Console.WriteLine("5.Press 5 to display Vehicle by id.");
                Console.WriteLine("6.Press 6 to add Vehicle on parking.");
                Console.WriteLine("7.Press 7 to remove Vehicle on parking.");
                Console.WriteLine("8.Press 8 to display last transactions.");
                Console.WriteLine("9.Press 9 to all logged transactions.");
                Console.WriteLine("10.Press 10 to top up Vehicle balance.");
                Console.WriteLine("Press 'e' and Enter to close the app.");
                
                switch (Console.ReadLine())
                {
                    case "1":
                        Console.WriteLine($"Current balance = {_client.GetStringAsync("https://localhost:44321/api/parking/balance").Result}");
                        break;
                    case "2":
                        
                        Console.WriteLine($"Capacity = {_client.GetStringAsync("https://localhost:44321/api/parking/capacity").Result}");
                        break;
                    case "3":
                        Console.WriteLine($"Parking has {_client.GetStringAsync("https://localhost:44321/api/parking/freePlaces").Result} free places.");
                        break;
                    case "4":
                        Console.WriteLine("List of all Vehicle that are on parking:");
                        var vehicles = _client.GetStringAsync("https://localhost:44321/api/vehicles").Result;
                        Console.WriteLine(vehicles);
                        break;
                    case "5":
                        try
                        {
                            Console.WriteLine("Input vehicle id(Format: AA-1111-AA, where A is any uppercase letter, and 1 is any digit):");
                            string idveh = Console.ReadLine();
                            var veh = _client.GetStringAsync("https://localhost:44321/api/vehicles/" + idveh).Result;
                            Console.WriteLine(veh);
                        }
                        catch(Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        break;
                        
                    case "6":
                        Console.WriteLine("Process of adding vehicle:");
                        try
                        {
                            Console.WriteLine("Input vehicle id(Format: AA-1111-AA, where A is any uppercase letter, and 1 is any digit):");
                            string id = Console.ReadLine();
                            Console.WriteLine("Input vehicle type. Choose one from : PassengerCar, Truck, Bus, Motorcycle:");

                            int.TryParse(Console.ReadLine(), out int type);
                            
                      
                            Console.WriteLine("Input vehicle initial balance (must be >= 0)");
                            string balanceInput = Console.ReadLine();
                            decimal balance = 0;
                            decimal.TryParse(balanceInput, out balance);

                            var newVehicle = new CreateNewVehicle { Id = id, VehicleType = (VehicleType)type, Balance = balance };
                        

                           
                            var t = await _client.PostAsJsonAsync("https://localhost:44321/api/vehicles/", newVehicle);

                            Vehicle R = t.Content.ReadAsAsync<Vehicle>().Result;
                            Console.WriteLine(R);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        break;

                       
                    case "7":
                        Console.WriteLine("Process of removing vehicle:");
                        try
                        {
                            Console.WriteLine("Input vehicle id(Format: AA-1111-AA, where A is any uppercase letter, and 1 is any digit):");
                            string id = Console.ReadLine();
                         
                            await _client.DeleteAsync("https://localhost:44321/api/vehicles/" + id);
                            Console.WriteLine($"Vehicle with id: {id} was removed from parking");
                        }
                        catch (InvalidOperationException e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        break;
                        
                    case "8":

                        Console.WriteLine($"{ _client.GetStringAsync("https://localhost:44321/api/transactions/last").Result }");

                        break;
                    case "9":

                        Console.WriteLine("List of all transactions that are in log file:");
                        try
                        {
                            Console.WriteLine($"{ _client.GetStringAsync("https://localhost:44321/api/transactions/all").Result }");
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        break;


                    case "10":
                        Console.WriteLine("Process of topping up vehicle's balance:");
                        try
                        {
                            Console.WriteLine("Input vehicle id(Format: AA-1111-AA, where A is any uppercase letter, and 1 is any digit):");
                            string id = Console.ReadLine();
                            Console.WriteLine("Input amount you want to add (must be >= 0");
                            string balanceInput = Console.ReadLine();
                            decimal balance = 0;
                            decimal.TryParse(balanceInput, out balance);
                            
                          
                            
                            var newVehicle = new TopUpVehicle { Id = id,  Sum = balance };
                            var response1 = await _client.PutAsJsonAsync("https://localhost:44321/api/transactions/topUpVehicle", newVehicle);
                            Console.WriteLine($"Vehicle's balance with id: {id} was successfully topped up");
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        break;
                    case "e":
                        endApp = true;
                        break;
                    default:
                        Console.Write("This is not valid input. Please enter value in range [1;10].\n\n ");
                        break;
                }
                
            }
        }
    }
}
